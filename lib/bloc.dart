import 'dart:async';

import 'package:last_bloc_changecolor_with_text_on/project.dart';
import 'package:last_bloc_changecolor_with_text_on/state.dart';

class Bloc {

  final _streamController1 = StreamController<StateN>(); /// morkovka   вона  тобто цей метод заставляє   StreamBuilder перемальовувати state   ЦЕЙ МЕТОД ЗАВЖДИ ТАК ПИШЕТЬСЯ
  Stream <StateN> get getStreamController1 => _streamController1.stream; /// zovnishniy dostup do morkovki тобто це те що бачить і до чого звертається файл  project

  final _streamController2 = StreamController<StateN>(); /// morkovka   вона  тобто цей метод заставляє   StreamBuilder перемальовувати state   ЦЕЙ МЕТОД ЗАВЖДИ ТАК ПИШЕТЬСЯ
  Stream <StateN> get getStreamController2 => _streamController2.stream;



  void dispose(){
    _streamController1.close();
    _streamController2.close();
      /// закриваємо даний метод

  }

  bool kolirVidraIsRed = true;   /// створюємо нашу булеву перемінну

  bool kolirVidraIsCyan = true;  /// створюємо нашу булеву перемінну

  RRR firstValue = RRR.green;

  // Future<void>  event () async{
  //   /// проголошуємо метод  тобто нашу подію event і прописуємо її ,повязуємо у FloatingActionButton файла project,  повязуємо натискання кнопки FloatingActionButton (event) з блоком
  //
  //   if (kolirVidraIsRed == true)   {
  //     kolirVidraIsRed = false;
  //   } else if (kolirVidraIsRed == false) {
  //     kolirVidraIsRed = true ;
  //   }
  //
  //   if (!_streamController1.isClosed) {
  //     _streamController1.sink.add(StateN.changeColor(kolirVidraIsRed));
  //   }
  // }
void changeColor(){
  if(firstValue == RRR.green){
    firstValue = RRR.yellow;
  }else if(firstValue == RRR.yellow){
    firstValue = RRR.green;
  }
  if (!_streamController1.isClosed) {
        _streamController1.sink.add(StateN.changeColor(firstValue));
      }
}
void changeText(){
  if (!_streamController1.isClosed) {
    _streamController1.sink.add(StateN.changeColor(RRR.text));
  }
}
  void  event1 ()   {
    /// проголошуємо метод  тобто нашу подію event і прописуємо її ,повязуємо у FloatingActionButton файла project,  повязуємо натискання кнопки FloatingActionButton (event) з блоком

    if (kolirVidraIsCyan == true) {
      kolirVidraIsCyan = false;
    } else if (kolirVidraIsCyan == false) {
      kolirVidraIsCyan = true;
    }

    if (!_streamController2.isClosed) {
      _streamController2.sink.add(StateN.changeColor1(kolirVidraIsCyan));
    }
  }

}
