import 'package:last_bloc_changecolor_with_text_on/project.dart';

class StateN {
  StateN();

  factory StateN.changeColor(RRR isRed) = ColorProjectStateRed;

  factory StateN.changeColor1(bool isCyan) = ColorProjectStateCyan;

}

class ColorProjectStateRed extends StateN {
  final RRR isRed;
  ColorProjectStateRed(this.isRed);}


class ColorProjectStateCyan extends StateN {
  final bool isCyan;
  ColorProjectStateCyan(this.isCyan);}


